const fs=require('fs');
const path=require('path');

function createdirectory(directoryPath,callback)
{
//  fs.mkdir ->It is used to create directories and any necessary parent directories
// recursive:true  ->  It is useful for creating nested directories   
    fs.mkdir(directoryPath,(err)=>{
        if(err)
        {
            if(err.code === 'EEXIST')
            {
                console.log(`Directory ${directoryPath} already exists`);
                callback(null);
            }
            else{
                callback(err);
            }
        }
        else{
            console.log(`Directory ${directoryPath}  is created Successfully `);
            callback(null);
        }
    })
}


// It will generate randome id,name,value 

function genearateRandomdata()
{

    return{
        id : Math.floor(Math.random() * 1000),
        name : `Item-${Math.floor(Math.random() * 100) }`,
        value : Math.random() *100
    };
}

function addRandomdatatoJSonfiles(directoryPath,numberofFiles,callback)
{
    let filePaths=[];
    
    
    function adddatatofile(i)
    {
        if(i > numberofFiles)
        {
            callback(null,filePaths);
            return;
        }
        
        //   Its the name of file that to be make       
        const filename=`file${i}.json`;
        //  path.join is used to make sure the file path is formatted correctly       
        const filepath=path.join(directoryPath,filename);
        const jsondata=genearateRandomdata();

//  It is used to convert js object into Jsonformatted string  
// JSON.stringify (filename,properties to be included,space separated)      
        const jsonstring= JSON.stringify(jsondata,null,2);

    fs.writeFile(filepath,jsonstring,(err)=>{
        if(err)
        {
            callback(err);
            return;
        }
            console.log(`Data added to ${filename}`);
            filePaths.push(filepath);
            adddatatofile(i+1);

            })
            
        }
        // Starting adding data to files
        adddatatofile(1);
        
    }

// Function  for deleting the random fiels generated

function deletefiles(filepaths,callback)
{
    // If we have to delete a single file 
    function deletefile(i)
    {
        if(i >= filepaths.length)
        {
            callback(null);
            return;
        }

        fs.unlink(filepaths[i], (err)=>{
            if(err)
           {
               callback(err);
               return;
            } 
            console.log(`File ${filepaths[i]} Deleted Successfully`);
           deletefile(i+1);
        });
    }

    // Starting the deleting Process 
    deletefile(0);
    
}

module .exports ={
    createdirectory,
    addRandomdatatoJSonfiles,
    deletefiles
};