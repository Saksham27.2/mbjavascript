
const {createdirectory , addRandomdatatoJSonfiles , deletefiles }=require("../fs-problem1");

const directorypath="./data";
const numberoffiles=5;

createdirectory(directorypath,(err)=>{
    if(err)
    {
        console.log(`Error Creating directory : ${err}`);
        return;
    }



// Adding Random data to JSON files within the directory 
    addRandomdatatoJSonfiles(directorypath,numberoffiles,(err,filePaths)=>{
        if(err)
        {
            console.log(`Error creating the files `);
            return;
        }        

        deletefiles(filePaths,(err)=>{
            if(err)
            {
                console.log(`Error deleting the files ${err}`);
                return;
            }
            console.log(`All files deleted Successfully`);
        })
    })
})